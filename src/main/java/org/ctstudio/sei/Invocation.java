package org.ctstudio.sei;

import java.util.Arrays;

public class Invocation {
  private String method;
  private String[] extraParams;

  public Invocation(String method, String... extraParams) {
    this.method = method;
    this.extraParams = extraParams;
  }

  public Invocation(String method) {
    this(method, null);
  }

  public Invocation() {
    this(null, null);
  }

  public String getMethod() {
    return method;
  }

  public String[] getExtraParams() {
    return extraParams;
  }

  @Override
  public int hashCode() {
    return method.hashCode() + (extraParams == null ? 0 : Arrays.hashCode(extraParams));
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false;
    if (!this.getClass().isInstance(obj)) return false;
    if (this.hashCode() != obj.hashCode()) return false;
    Invocation another = (Invocation) obj;
    return this.method.equals(another.method) && Arrays.equals(this.extraParams, another.extraParams);
  }

  @Override
  public String toString() {
    return extraParams == null || extraParams.length == 0 ? method : String.format("%s:%s", method, String.join(",", extraParams));
  }
}
