package org.ctstudio.sei;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

public class SimpleExpressionInterpreterTester {
  static final String template = "亲爱的${uid|userInfo|prop:name}${uid|userInfo|prop:gender|genderName}\n" +
      "  你好！欢迎加入不存在公司，你的部门是${uid|department|prop:name}，岗位职级${uid|position|prop:name}…\n" +
      "  人事部 HR ${my|prop:name}${my|prop:gender|genderName}\n";
  static final String value = "亲爱的李四女士\n" +
      "  你好！欢迎加入不存在公司，你的部门是互联网行销部，岗位职级产品经理T1…\n" +
      "  人事部 HR 张三先生\n";

  private SimpleExpressionInterpreter testObj = new SimpleExpressionInterpreter(new DemoFunctionProvider());

  @Test
  public void testEval() {
    Assert.assertEquals(value, testObj.eval(template));
    Assert.assertEquals(value + "...", testObj.eval(template + "..."));
    Assert.assertEquals("", testObj.eval(""));
    Assert.assertNull(testObj.eval(null));
  }

  @Test
  public void testNextDivider() {
    Assert.assertEquals(-1, testObj.nextDivider("{a}", testObj.expressionStart, 0));
    Assert.assertEquals(0, testObj.nextDivider("${a}", testObj.expressionStart, 0));
    Assert.assertEquals(2, testObj.nextDivider("\\$${a}", testObj.expressionStart, 0));
    Assert.assertEquals(4, testObj.nextDivider("${a}${b}", testObj.expressionStart, 1));
    Assert.assertEquals(3, testObj.nextDivider("${a}${b}", testObj.expressionEnd, 1));
    Assert.assertEquals(8, testObj.nextDivider("${a\\}${b}", testObj.expressionEnd, 1));
    Assert.assertEquals(-1, testObj.nextDivider("${a\\}${b", testObj.expressionEnd, 1));
  }

  @Test
  public void testFindExpressions() {
    Assert.assertEquals(Collections.EMPTY_LIST, testObj.findExpressions("{a}"));
    Assert.assertEquals(Collections.singletonList("${a}"), testObj.findExpressions("${a}"));
    Assert.assertEquals(Collections.singletonList("${a}"), testObj.findExpressions("\\$${a}"));
    Assert.assertEquals(Arrays.asList("${a}", "${b}"), testObj.findExpressions("${a}${b}"));
    Assert.assertEquals(Arrays.asList("${a}", "${b}"), testObj.findExpressions("Hello ${a}, world${b}"));
    Assert.assertEquals(Collections.singletonList("${a\\}${b}"), testObj.findExpressions("${a\\}${b}"));
    Assert.assertEquals(Collections.EMPTY_LIST, testObj.findExpressions("${a\\}${b"));
    Assert.assertEquals(Arrays.asList("${uid|userInfo|prop:name}", "${uid|userInfo|prop:gender|genderName}",
        "${uid|department|prop:name}", "${uid|position|prop:name}",
        "${my|prop:name}", "${my|prop:gender|genderName}"), testObj.findExpressions(template));
  }

  @Test
  public void testSplit() {
    Assert.assertNull(testObj.split(null, "|"));
    Assert.assertArrayEquals(new String[]{"a|b"}, testObj.split("a|b", null));
    Assert.assertArrayEquals(new String[]{"a|b"}, testObj.split("a|b", ""));
    Assert.assertArrayEquals(new String[]{"a", "b"}, testObj.split("a|b", "|"));
    Assert.assertArrayEquals(new String[]{"ab", "cd:ef,gh"}, testObj.split("ab|cd:ef,gh", "|"));
    Assert.assertArrayEquals(new String[]{"ab", "cd", "ef", "gh"}, testObj.split("ab,cd,ef,gh", ","));
  }

  @Test
  public void testParseInvocations() {
    Assert.assertEquals(Collections.EMPTY_LIST, testObj.parseInvocations(""));
    Assert.assertEquals(Collections.singletonList(new Invocation("a")), testObj.parseInvocations("${a}"));
    Assert.assertEquals(Arrays.asList(new Invocation("a"), new Invocation("b"), new Invocation("c")), testObj.parseInvocations("${a|b|c}"));
    Assert.assertEquals(Arrays.asList(new Invocation("uid"), new Invocation("userInfo"),
        new Invocation("prop", "name")), testObj.parseInvocations("${uid|userInfo|prop:name}"));
  }

  @Test
  public void testEvalExpression() {
    Assert.assertEquals("李四", testObj.evalExpression("${uid|userInfo|prop:name}"));
    Assert.assertEquals("女士", testObj.evalExpression("${uid|userInfo|prop:gender|genderName}"));
    Assert.assertEquals("先生", testObj.evalExpression("${my|prop:gender|genderName}"));
    Assert.assertEquals("", testObj.evalExpression("${my1|prop:gender|genderName}"));
  }
}
