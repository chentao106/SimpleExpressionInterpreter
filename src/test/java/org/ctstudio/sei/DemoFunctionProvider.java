package org.ctstudio.sei;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DemoFunctionProvider {
  private Map<String, Object> callParameters;//模拟实时传入的参数
  private Map<String, Map<String, ?>> demoDB;//模拟数据库中的数据

  public DemoFunctionProvider() {
    callParameters = new HashMap<>();
    Map<String, Object> my = new HashMap<>();
    my.put("id", 0);
    my.put("name", "张三");
    my.put("gender", 1);
    callParameters.put("my", my);
    callParameters.put("uid", 1);

    demoDB = new HashMap<>();
    Map<String, Object> you = new HashMap<>();
    you.put("id", 1);
    you.put("name", "李四");
    you.put("gender", 2);
    demoDB.put(String.format("user:%d", 1), you);
    Map<String, Object> yourDepartment = Collections.singletonMap("name", "互联网行销部");
    demoDB.put(String.format("user:%d:department", 1), yourDepartment);
    Map<String, Object> yourPosition = Collections.singletonMap("name", "产品经理T1");
    demoDB.put(String.format("user:%d:position", 1), yourPosition);
  }

  public Object uid() {
    return callParameters.get("uid");
  }

  public Object my() {
    return callParameters.get("my");
  }

  public Map<String, ?> userInfo(int uid) {
    return demoDB.get(String.format("user:%d", uid));
  }

  public Map<String, ?> department(int uid) {
    return demoDB.get(String.format("user:%d:department", uid));
  }

  public Map<String, ?> position(int uid) {
    return demoDB.get(String.format("user:%d:position", uid));
  }

  public Object prop(Map<String, ?> map, String propName) {
    return map.get(propName);
  }

  public String genderName(int gender) {
    switch (gender) {
      case 1:
        return "先生";
      case 2:
        return "女士";
      default:
        return "";
    }
  }
}
